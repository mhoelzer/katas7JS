const letters = ["b", "i", "k", "o", "q", "v", "x", "z"];
const numbers = [1, 2, 3, 5, 8, 13, 21, 34, 55];

// 1. forEach
console.log("FOREACH");
function forEach(arrayForEach, callbackForEach) {
    for (let counter = 0; counter < arrayForEach.length; counter++) {
        const currentValue = arrayForEach[counter];
        callbackForEach(currentValue, counter, arrayForEach);
    }
}
forEach(letters, function(letter, index, array) {
    const letterIsVowel = letter === "a" || letter === "e" || letter === "i" || letter === "o" || letter === "u";
    const letterType = letterIsVowel ? "vowel" : "consonant";
    console.log(`The letter '${letter}' at position ${index + 1}/index ${index} of [${array}] is a ${letterType}.`);
});

// 2. map
console.log("MAP");
function map(arrayMap, callbackMap) {
    for (let counter = 0; counter < arrayMap.length; counter++) {
        const currentValue = arrayMap[counter];
        arrayMap[counter] = callbackMap(currentValue, counter, arrayMap);
    } return arrayMap;
}
console.log(`[${numbers}] is now [${map(numbers, number => number + 3)}]`);



// 3. some
console.log("SOME");
const someArrayExample = ["big jumbo shrimp", "happy dog", "supercalifragilisticexpialidocious"];
function some(arraySome, callbackSome) {
    for (let counter = 0; counter < arraySome.length; counter++) {
        const currentValue = arraySome[counter];
        if(callbackSome(currentValue, counter, arraySome)) return true;
    } return false;
}
console.log(`Does any item in [${someArrayExample}] contain under 20 characters? ${some(someArrayExample, item => item.length <= 20)}`);
console.log(`Does any item in [${someArrayExample}] contain under 4 characters? ${some(someArrayExample, item => item.length < 4)}`);



const findAndFindIndexArrayExample = ["gogo", "nono", "yaboi", "beep boop bep bop", "chimichanga"];

// 4. find
console.log("FIND");
function find(arrayFind, callbackFind) {
    for (let counter = 0; counter < arrayFind.length; counter++) {
        const currentValue = arrayFind[counter];
        if(callbackFind(currentValue) === true) return currentValue
    }
}
console.log(`First item in [${findAndFindIndexArrayExample}] with the letter "I" is ${find(findAndFindIndexArrayExample, item => item.includes("i"))}`);
console.log(`First item in [${findAndFindIndexArrayExample}] with " " is ${find(findAndFindIndexArrayExample, item => item.includes(" "))}`);

// 5. findIndex
console.log("FINDINDEX");
function findIndex(arrayFindIndex, callbackFindIndex) {
    for (let counter = 0; counter < arrayFindIndex.length; counter++) {
        const currentValue = arrayFindIndex[counter];
        if(callbackFindIndex(currentValue, counter, arrayFindIndex)) return counter
    } return -1;
}
console.log(`The index of [${findAndFindIndexArrayExample}] with fewer than 2 characters is: ${findIndex(findAndFindIndexArrayExample, item => item.length < 2)}`);
console.log(`The index of [${findAndFindIndexArrayExample}] with more than 10 characters is: ${findIndex(findAndFindIndexArrayExample, item => item.length > 10)}`);
// console.log(find(findArrayExample, item => item.includes("i")))



// 6. every
console.log("EVERY");
const everyArrayExample = [1, 2, 3, 4, 5, 6, 666];
function every(arrayEvery, callbackEvery) {
    for (let counter = 0; counter < arrayEvery.length; counter++) {
        const currentValue = arrayEvery[counter];
        if(!callbackEvery(currentValue, counter, arrayEvery)) return false;
    } return true;
}
console.log(`All of [${everyArrayExample}] <= 667: ${every(everyArrayExample, number => number <= 667)}`);
console.log(`All of [${everyArrayExample}] < 22: ${every(everyArrayExample, number => number < 22)}`);



// 7. filter
console.log("FILTER");
const filterArrayExample = ["Shaun of the Dead", "Hot Fuzz", "The World's End"];
function filter(arrayFilter, callbackFilter) {
    const newArrayFilter = [];
    for (let counter = 0; counter < arrayFilter.length; counter++) {
        const currentValue = arrayFilter[counter];
        if(callbackFilter(currentValue) === true) {
            newArrayFilter.push(currentValue);
        }
    } return newArrayFilter;
}
console.log(`Movies in [${filterArrayExample}] with the word "the" is ${filter(filterArrayExample, movie => movie.toLowerCase().includes("the"))}`);
console.log(`Movies in [${filterArrayExample}] with the letter "u" is ${filter(filterArrayExample, movie => movie.includes("u"))}`);